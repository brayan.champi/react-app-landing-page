import React from 'react';
import ReactDOM from 'react-dom';
import HomePage from "./page/HomePage";
import './page/style/style.css';


const container = document.getElementById('app');

// ReactDOM.render(__que__, __donde__);
ReactDOM.render(<HomePage />, container);
