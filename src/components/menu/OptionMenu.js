import React from 'react';
import {OptionLink} from "../../elements/menu/MenuForm";


function OptionMenu(props) {
    const {option} = props
    return (
        <OptionLink href="#">{option}</OptionLink>
    )
}

export default OptionMenu;
