import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faHeart} from "@fortawesome/free-solid-svg-icons";
import {ContentImageSong, DescriptionSong, DivSong, LikeSong} from "../../elements/music/MusicForm";


function Song(props) {
    const {iconSong, song, singer, like} = props
    return (
        <DivSong>
            <ContentImageSong>
                <FontAwesomeIcon icon={iconSong}/>
            </ContentImageSong>
            <DescriptionSong>
                <span>{song}</span>
                <span>{singer}</span>
            </DescriptionSong>
            <LikeSong>
                <FontAwesomeIcon icon={faHeart}/>
                <span>{like}</span>
            </LikeSong>
        </DivSong>
    )
}

export default Song;
