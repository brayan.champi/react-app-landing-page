import React from 'react';
import {ContentCategory} from "../../elements/music/MusicForm";


function Category() {
    return (
        <ContentCategory>
            <h2 style={{color: "white"}}>Verano</h2>
            <a href="#">Ver todo</a>
        </ContentCategory>
    )
}

export default Category;
