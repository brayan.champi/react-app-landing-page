import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBars, faSearch,} from "@fortawesome/free-solid-svg-icons";
import profilePicture from "../../img/foto_perfil.jpg"
import {ContentSearch, GroupInput, IconSearch, Image, Input} from "../../elements/music/MusicForm";


function SearchForm() {
    return (
        <div>
            <ContentSearch>
                <FontAwesomeIcon icon={faBars}/>
                <GroupInput>
                    <Input type="text" placeholder="Buscar album, canciones..."/>
                    <IconSearch icon={faSearch}/>
                </GroupInput>
                <Image src={profilePicture} alt="profilePicture"/>
            </ContentSearch>
            <h2 style={{color: "white"}}>Sugerencias para ti</h2>
        </div>
    )
}

export default SearchForm;
