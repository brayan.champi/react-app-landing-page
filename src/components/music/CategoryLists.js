import React from 'react';
import {ContentPlayList} from "../../elements/music/MusicForm";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


function CategoryLists(props) {
    const {iconCategory} = props
    return (
        <ContentPlayList>
            <FontAwesomeIcon icon={iconCategory}/>
        </ContentPlayList>
    )
}

export default CategoryLists;
