import React from 'react';
import SearchForm from '../components/music/SearchForm'
import OptionMenu from '../components/menu/OptionMenu'
import Song from '../components/music/Song'
import Category from '../components/music/Category'
import CategoryLists from '../components/music/CategoryLists'
import {Container, ContentOption, ContentPlayList, ContentSong} from "../elements/StyleForm";
import {faCarrot, faGlassCheers, faSkullCrossbones, faStar, faUmbrellaBeach} from "@fortawesome/free-solid-svg-icons";


function HomePage() {
    return (
        <Container>
            <SearchForm/>
            <ContentOption>
                <OptionMenu option="Canciones"/>
                <OptionMenu option="Playlists"/>
            </ContentOption>
            <ContentSong>
                <Song iconSong={faCarrot} song="La Canción" singer="J Balvin x Bad Bunny" like="9970"/>
                <Song iconSong={faSkullCrossbones} song="Tatto" singer="Rauw Alejandro & Camilo" like="1080"/>
                <Song iconSong={faStar} song="Deseandote" singer="cFrankie Ruiz" like="1800"/>
            </ContentSong>
            <Category/>
            <ContentPlayList>
                <CategoryLists iconCategory={faUmbrellaBeach}/>
                <CategoryLists iconCategory={faGlassCheers}/>
            </ContentPlayList>
        </Container>
    )
}

export default HomePage;
