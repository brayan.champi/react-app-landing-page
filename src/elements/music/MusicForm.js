import styled from "styled-components";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


// SEARCH
const ContentSearch = styled.div`
    display: flex;
    justify-content: center;
    margin-bottom: 30px;
    
    svg:first-child {
        margin: auto;
        font-size: 22px;
        color: white;
    }
`;

const GroupInput = styled.div`
    position: relative;
    display: flex;
    z-index: 90;
    width: 100%;
    margin: 0 35px;
    input {
        box-sizing: border-box;
    }
`;

const Input = styled.input`
    width: 100%;
    background: #323232;
    border-radius: 20px;
    color: #eaeaea;
    height: 35px;
    line-height: 45px;
    padding: 0 0 0 36px;
    transition: .3s ease all;
    border: 3px solid transparent;
    
    &:focus {
        outline: none;
        box-shadow: 1px 0 10px rgba(186,186,186,0.4);
    }
    
    ::placeholder {
        color: #838383;
    }
`;

const IconSearch = styled(FontAwesomeIcon)`
    position: absolute;
    color: white;
    left: 0;
    top: 8px;
    padding: 1px 14px;
`;

const Image = styled.img`
    width: 35px;
    height: 35px;
    object-fit: cover;
    border-radius: 150px;
`;


// SONG
const DivSong = styled.div`
    display: flex;
    margin-top: 25px;
`;

const ContentImageSong = styled.div`
    width: 60px;
    height: 60px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 7px;
    background: linear-gradient(90deg, rgba(253,135,89,1) 0%, rgba(255,26,94,1) 100%);
    
    svg {
        font-size: 30px;
    }
`;

const DescriptionSong = styled.div`
    display: grid;
    margin-left: 17px;
    
    span:first-child {
        margin: auto 0 0;
        color: white;
        font-size: 15px;
    }
    
    span:last-child {
        margin: 0;
        color: #b5b5b5;
        font-size: 13px;
    }
`;

const LikeSong = styled.div`
    display: grid;
    align-items: center;   
    margin-left: auto; 
    
    svg {
        margin: auto 0 0 auto;
        color: white;
        font-size: 15px;
    }
    
    span {
        color: #b5b5b5;
        font-size: 14px;
    }
`;


// CATEGORY
const ContentCategory = styled.div`
    display: flex;
    align-items: center;
    margin-top: 10px;
    
    a {
        display: grid;
        align-items: center;
        margin: 0 0 0 auto;
        padding: 3px 3px 3px 3px; 
        font-size: 13px;
        text-decoration: none;
        text-align: center;
        color: #000000;
        background: white;
        border-radius: 20px;
        height: 25px;
        width: 70px;
    }
    
    a:hover {
        color: #eeeeee;
        background: #323232;
        transition: .5s ease all;
    }
    
    a:active {
        background: #585858;
    }
`;

const ContentPlayList = styled.div`
    width: 150px;
    height: 140px;
    display: flex;
    margin: auto;
    justify-content: center;
    align-items: center;
    border-radius: 7px;
    background: linear-gradient(90deg, rgba(89,253,251,1) 0%, rgba(60,127,182,1) 100%);
    
    svg {
        color: #ffffff;
        font-size: 70px;
    }
`;


export {
    ContentSearch,
    GroupInput,
    Input,
    IconSearch,
    Image,
    DivSong,
    ContentImageSong,
    DescriptionSong,
    LikeSong,
    ContentCategory,
    ContentPlayList
}
