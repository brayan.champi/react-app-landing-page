import styled from "styled-components";


const Container = styled.div`
    max-width: 350px;
    margin: auto;
    box-shadow: 30px 0 40px rgba(0, 0, 0, 0.1),
        -30px 0 40px rgba(0, 0, 0, 0.1);
    padding: 30px 20px 15px 20px;
    background: #000000;
`;

const ContentOption = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 25px;
    
    a:first-child {
        margin-right: 120px;   
    }
    
    a:link {
        color: #797a7d;
    }
    
    a:hover {
        color: #eeeeee;
        border-bottom: 5px solid #bb3351;
    }
    
    a:active{
        color: #d7d7d7;
        border-bottom: 5px solid #bb3351;
    }
`;

const ContentSong = styled.div`
    display: block;
    margin-top: 30px;
`;

const ContentPlayList = styled.div`
    display: flex;
    margin-top: 10px;
`;


export {Container, ContentOption, ContentSong, ContentPlayList}
