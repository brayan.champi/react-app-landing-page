import styled from "styled-components";


const OptionLink = styled.a `
    font-size: 1rem;
    text-decoration: none;
    color: #797a7d;
    border-bottom: 5px solid transparent
`;


export {OptionLink}
